-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 14, 2018 at 07:12 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ad`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `item_id` int(30) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(100) NOT NULL,
  `descr` varchar(350) NOT NULL,
  `pic` longblob NOT NULL,
  `price` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item_name`, `descr`, `pic`, `price`, `date`) VALUES
(23, 'Razer Ouroboros', 'gejmerski mis', 0x72617a65722d6f75726f626f726f732d6d6f7573652d6c6967687473322e6a7067, 150, '2018-04-14 07:30:35'),
(24, 'Razer Black Widow Ultimate', 'Gejmerska tastatura', 0x626c61636b77696e646f772d756c74696d6174652d323233783238302e6a7067, 120, '2018-04-14 07:31:59'),
(25, 'Razer Tartarus', 'Kontroler', 0x72617a65722d74617274617275732d6361726f7573656c2d76322e706e67, 200, '2018-04-14 07:32:56'),
(26, 'Razer talon', 'kontroler', 0x74616c6f6e2e6a7067, 170, '2018-04-14 07:33:27'),
(27, 'Razer Nabu', 'Smart Band', 0x727a725f6e6162755f7669657730375f677265656e5f75735f626c6b62672d3130303331313733372d6f7269672e6a7067, 220, '2018-04-14 07:34:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(30) NOT NULL,
  `uname` varchar(30) NOT NULL,
  `pass` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `uname`, `pass`) VALUES
(1, 'djomla', '1b13c6f926a61758d7732cdf7928060b');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
