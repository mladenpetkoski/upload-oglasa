<?php
  require("include/config.php");
  require("include/db.php");
  require("include/functions.php");

?>
<h2>Unos oglasa</h2>
<link rel="stylesheet" type="text/css" href="css/styles.css">
<link href="https://fonts.googleapis.com/css?family=VT323&amp;subset=latin-ext" rel="stylesheet">
<form action="realinsert.php" method="POST" enctype="multipart/form-data">
  <fieldset class="fieldset">
    <legend class="legend">Novi Oglas</legend>
    <label>Nazi Oglasa</label><br>
    <input type="text" name="name" value="" required="required"><br><br>
    <label>Cena</label><br>
    <input type="number" name="price" value="" required="required"><br><br>
    <label>Opis</label><br>
    <textarea type="text" name="description"  value="" required="required"></textarea><br><br>
    <label>Upload Slike</label><br>
    <input type="file" name="file" value="" required="required"><br><br>
    <input type="submit" name="submitbutton" id="submitbutton" value="Unos"><br><br>
    <?php
    echo "<a href=\"index.php\">Nazad</a><br>";
    ?>
  </fieldset>
</form>